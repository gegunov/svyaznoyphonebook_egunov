$.fn.myPagination = function (options) {
    if (!options)options = {perPage: 20};
    var rowsPerPage = options.perPage;
    var rows = $('.numberRow');
    var pageQty = Math.ceil(rows.length / rowsPerPage);
    var currentPage = 1;
    if (pageQty < 2) {
        rows.css('display', 'block');
    } else {
        var navTabs = $('.navTabs');
        $('#data').append('<div class="navigation"></div>');
        $('.navigation').append('<div class="navTabs leftBrackets"><p>Begin</p></div>');
        $('.navigation').append('<div class="navTabs leftBrackets"><p>Previous</p></div>');
        for (var i = 0; i < pageQty && i < 3; i++) {
            var val = i + 1;
            $('.navigation').append('<div class="navTabs num"><p> </p></div>');
        }

        $('.navigation').append('<div class="navTabs rightBrackets"><p>Next</p></div>');
        $('.navigation').append('<div class="navTabs rightBrackets"><p>Last</p></div>');
        fillTabs(currentPage);
        showCurrPage();
        $('.navTabs').on('click', (function () {
            var clicked = $(this).text();
            switch (clicked) {
                case 'Begin':
                    currentPage = 1;
                    break;
                case 'Previous':
                    if (currentPage > 1) {
                        currentPage--;
                    }
                    break;
                case 'Next':
                    if (currentPage < pageQty) {
                        currentPage++;
                    }
                    break;
                case 'Last':
                    currentPage = pageQty;
                    break;
                default :
                    currentPage = clicked;
            }
            fillTabs(currentPage);
            showCurrPage();
        }));
    }

    //function cursorTransformation(){
    //    $('.navTabs').onmouseover()
    //}

    function showCurrPage() {
        rows.css('display', 'none');
        var toDispl = rows.slice((currentPage - 1) * rowsPerPage, ((currentPage - 1) * rowsPerPage) + rowsPerPage);
        toDispl.css('display', 'inline');
    }

    function fillTabs(currPage) {

        if (currPage > pageQty - 3) {
            currPage = pageQty - 2;
        }
        if (currPage < 1) {
            currPage = 1
        }
        $('.num p').each(function () {
            $(this).text(currPage++);
        });
    }
};


