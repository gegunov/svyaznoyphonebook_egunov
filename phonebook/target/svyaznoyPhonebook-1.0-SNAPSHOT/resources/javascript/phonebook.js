$(function () {
    loadNumbers();

    $('#saveNums').click(function () {
        $(this).text('Данные загружаются');
        var lineSeparator = getLineBreakSequence();
        var nums = [];
        var errors = [];
        var strArray = $('#numberinputs').val();
        strArray = strArray.split(lineSeparator);
        var errorStrings = '';
        $.each(strArray, function (i, str) {
            str = str.replace(/\s+/g, '');
            /^79\d{9}$/.test(str) ? nums.push(str) : errors.push(str);
        });
        $.each(errors, function (i, item) {
            errorStrings = errorStrings.concat(item + lineSeparator);
        });
        $('.navigation').remove();
        $('#errorData').val(errorStrings);
        var list = {'nums': nums};
        $.ajax({
            url: 'saveNumbers',
            data: JSON.stringify(list),
            type: 'POST',
            contentType: "application/json",
            dataType: 'json',
            success: getNumbers
        });
    });
});

function loadNumbers() {
    var posting = $.post('loadNumbers');
    posting.done(getNumbers);
}

function getNumbers(data) {
    $('#numberinputs').val('');
    $('.numberRow').remove();
    var numbers = [];

    $.each(data.phoneNumbers, function (i, phonenumber) {
        var date = new Date(phonenumber.date);
        numbers.push('<tr class="numberRow">');
        numbers.push('<td class="numberCell" id="'+ phonenumber.id +'">' + phonenumber.number + '</td>');
        numbers.push('<td class="dataCell">' + date.toDateString() + '</td>');
        numbers.push('<td class="edit" id="'+ phonenumber.id +'"><img src="'+ context +
        '/resources/images/edit.png"/></td><td class="del" id="'+ phonenumber.id +'"><img src="'+ context + '/resources/images/delete.png"/></td>');
        numbers.push('</tr>');
    });
    $('#numbers').append(numbers.join(''));
    $('#numbers').myPagination();
    $('#saveNums').text('Сохранить');

    $('.edit').click(function(){
        var id = $(this).attr('id');
        var numberValue = $('.numberCell[id =' + id +']').text();
        $('#redInput').val(numberValue);
        $('#redaktor').css('display','block');
        $('#update').one('click',function(){
           var updated = $('#redInput').val();
            if(/^79\d{9}$/.test(updated) && (!contains(updated, numberValue))){
                var posting = $.post('update',{'id':id, 'updated' : updated});
                $('#redaktor').css('display','none');
                loadNumbers();
            }else {
                alert('Некорректные данные или такой номер уже имеется в базе');
                return false;
            }
        });
        $('#cancel').one('click',function() {
            $('#redInput').val('');
            $('#redaktor').css('display','none');
        });
    });

    $('.del').click(function(){
        var id = $(this).attr('id');
        $.post('removeNumber',{'id':id});
        loadNumbers();
    });
}
function contains(text, numberValue){
    var inputs = [];
    $('.numberCell').each(function(){
        if($(this).text()==numberValue){
            return;
        }
        inputs.push($(this).text());
    });
    var cont = $.inArray( text, inputs ) > -1;
    return  cont;
}

function getLineBreakSequence() {
    var div, ta, text;
    div = document.createElement("div");
    div.innerHTML = "<textarea>one\ntwo</textarea>";
    ta = div.firstChild;
    text = ta.value;
    return text.indexOf("\r") >= 0 ? "\r\n" : "\n";
}