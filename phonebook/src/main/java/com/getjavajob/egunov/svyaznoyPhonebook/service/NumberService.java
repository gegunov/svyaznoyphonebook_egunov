package com.getjavajob.egunov.svyaznoyPhonebook.service;

import com.getjavajob.egunov.svyaznoyPhonebook.dao.JDBCTemplateDao;
import com.getjavajob.egunov.svyaznoyPhonebook.models.PhoneNumber;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class NumberService {

    @Autowired
    private JDBCTemplateDao numberDao;

    public PhoneNumber get(int id) {
        return numberDao.get(id);
    }

    public List<PhoneNumber> getAll() {
        return numberDao.getAll();
    }

    @Transactional
    public void add(PhoneNumber phoneNumber) {
        numberDao.add(phoneNumber);
    }

    @Transactional
    public void update(int id, String value) {
        numberDao.update(id, value);
    }

    @Transactional
    public void remove(int id) {
        numberDao.remove(id);
    }

}
