package com.getjavajob.egunov.svyaznoyPhonebook.models;

import org.codehaus.jackson.annotate.JsonProperty;

import java.util.ArrayList;
import java.util.List;

public class ResponseForm {
    @JsonProperty
    private List<PhoneNumber> phoneNumbers = new ArrayList<>();

    public List<PhoneNumber> getPhoneNumbers() {
        return phoneNumbers;
    }

    public void setPhoneNumbers(List<PhoneNumber> phoneNumbers) {
        this.phoneNumbers = phoneNumbers;
    }
}
