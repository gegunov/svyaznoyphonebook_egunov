package com.getjavajob.egunov.svyaznoyPhonebook.dao;

import java.util.List;

public abstract class AbstractDao<T> {
    public abstract T get(int id);

    public abstract List<T> getAll();

    public abstract void add(T t);

    public abstract void update(int id, String newValue);

    public abstract void remove(int id);
}
