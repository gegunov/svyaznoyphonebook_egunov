package com.getjavajob.egunov.svyaznoyPhonebook.controller;

import com.getjavajob.egunov.svyaznoyPhonebook.models.PhoneNumber;
import com.getjavajob.egunov.svyaznoyPhonebook.models.RequestForm;
import com.getjavajob.egunov.svyaznoyPhonebook.models.ResponseForm;
import com.getjavajob.egunov.svyaznoyPhonebook.service.NumberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class PhonebookController {
    @Autowired
    private NumberService numberService;


    @RequestMapping("/MainPage")
    public String mainPage() {
        return "mainPage";
    }

    @RequestMapping(value = "/loadNumbers")
    public
    @ResponseBody
    ResponseForm loadNumbers() {
        ResponseForm result = new ResponseForm();
        result.setPhoneNumbers(numberService.getAll());
        return result;
    }

    @RequestMapping("/getNum")
    public
    @ResponseBody
    PhoneNumber getNum(@RequestBody int id) {
        return numberService.get(id);
    }

    @RequestMapping(value = "/saveNumbers")
    public
    @ResponseBody
    ResponseForm saveNumbers(@RequestBody RequestForm requestForm) {
        for (String num : requestForm.getNums()) {
            try {

                numberService.add(new PhoneNumber(num));
            } catch (DataIntegrityViolationException e) {

            }
        }
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ResponseForm result = loadNumbers();
        return result;
    }

    @RequestMapping("/update")
    public
    @ResponseBody
    int updateNumber(@ModelAttribute(value = "id") Integer id, @ModelAttribute(value = "updated") String number) {
        numberService.update(id, number);
        int result = 1;
        return result;
    }

    @RequestMapping("/removeNumber")
    public
    @ResponseBody
    int removeNumber(@ModelAttribute(value = "id") Integer id) {
        numberService.remove(id);
        int result = 1;
        return result;
    }

}

