package com.getjavajob.egunov.svyaznoyPhonebook.models;

import java.util.Date;

public class PhoneNumber {
    private int id;
    private String number;
    private Date date;

    public PhoneNumber(String number) {
        this.number = number;
        this.date = new Date();
    }

    public PhoneNumber() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhoneNumber)) return false;

        PhoneNumber phoneNumber = (PhoneNumber) o;

        if (!number.equals(phoneNumber.number)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return number.hashCode();
    }
}
