package com.getjavajob.egunov.svyaznoyPhonebook.dao;

import com.getjavajob.egunov.svyaznoyPhonebook.exceptions.DAOException;
import com.getjavajob.egunov.svyaznoyPhonebook.models.PhoneNumber;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class NumberDBDao extends AbstractDao<PhoneNumber> {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public PhoneNumber get(int id) {
        Session session = sessionFactory.getCurrentSession();
        return (PhoneNumber) session.get(PhoneNumber.class, id);
    }

    @Override
    public List<PhoneNumber> getAll() {
        List<PhoneNumber> result = null;
        try {
            Session session = sessionFactory.getCurrentSession();
            result = session.createCriteria(PhoneNumber.class).list();
        } catch (HibernateException e) {
            throw new DAOException();
        }
        return result;
    }


    @Override
    public void add(PhoneNumber phoneNumber) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.save(phoneNumber);
        } catch (HibernateException e) {
            throw new DAOException();
        }
    }

    @Override
    public void remove(int id) {
        try {
            Session session = sessionFactory.getCurrentSession();
            session.delete(get(id));
        } catch (HibernateException e) {
            throw new DAOException();
        }
    }

    @Override
    public void update(int id, String value) {
        try {
            Session session = sessionFactory.getCurrentSession();
            PhoneNumber pn = (PhoneNumber) session.get(PhoneNumber.class, id);
            pn.setNumber(value);
            session.update(pn);
        } catch (HibernateException e) {
            throw new DAOException();
        }
    }
}

