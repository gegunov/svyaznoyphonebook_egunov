<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>main Page</title>
    <link href="${pageContext.request.contextPath}/resources/css/style.css"
          rel="stylesheet" type="text/css"/>
    <link href="${pageContext.request.contextPath}/resources/css/bootstrap.css"
          rel="stylesheet" type="text/css"/>

    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/javascript/jquery-2.1.3.js">
    </script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/javascript/bootstrap.js">
    </script>
    <script type="text/javascript">
        var context = "${pageContext.request.contextPath}";
    </script>

    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/javascript/myPagination.js">
    </script>
    <script type="text/javascript"
            src="${pageContext.request.contextPath}/resources/javascript/phonebook.js">
    </script>

</head>
<body>
<div id="inputs" class="col-lg-3">
    <p><b/>Введите номера телефонов</p>
    <textarea id="numberinputs" class="form-control">

    </textarea>
    <button type="button" id="saveNums" data-loading-text="Данные загружаются..."
            class="btn btn-primary" autocomplete="off"> Сохранить
    </button>
</div>
<div id="errors" class="col-lg-3">
    <p>Ошибки в номерах</p>

    <textarea id="errorData" disabled>

    </textarea>
</div>
<div id="data" class="col-lg-4">
    <table id="resultTable">
        <thead>
        <tr>
            <th class="numberCell">Телефон</th>
            <th class="dataCell">Дата обработки</th>
        </tr>
        </thead>

        <tbody id="numbers">

        </tbody>
    </table>

</div>
<div id="redaktor">
    <div>
        <input type="text" id="redInput"/>
        <button type="button" class="btn btn-primary btn-lg btn-block" id="update">Сохранить</button>
        <button type="button" class="btn btn-primary btn-lg btn-block" id="cancel">Отмена</button>
    </div>

</div>

</body>
</html>
